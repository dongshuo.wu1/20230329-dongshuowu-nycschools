//
//  nyc_schoolsTests.swift
//  nyc-schoolsTests
//
//  Created by Dongshuo Wu on 4/6/23.
//

import XCTest
@testable import nyc_schools

final class nyc_schoolsTests: XCTestCase {
	
	var subject: SchoolListViewable! = nil

    override func setUpWithError() throws {
		subject = SchoolListViewModel(schoolList: [
			School(dbn: "dbn1", schoolName: "name1"),
			School(dbn: "dbn2", schoolName: "name2")
	 ])
		
    }

	func testSubjectNotNil() {
		XCTAssertNotNil(subject)
		
	}
	
	func testNumberOfItems() {
		XCTAssertEqual(2, subject.numberOfItems)
	}

}
