//
//  SATScore.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 4/5/23.
//

import Foundation

struct SATScore: Codable {
	let dbn: String
	let schoolName: String?
	let numOfSatTestTakers: String?
	let satCriticalReadingAvgScore: String?
	let satMathAvgScore: String?
	let satWritingAvgScore: String?
}
