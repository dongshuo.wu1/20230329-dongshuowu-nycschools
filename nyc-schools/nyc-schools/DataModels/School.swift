//
//  School.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/29/23.
//

import Foundation

struct School: Codable {
	let dbn: String
	let schoolName: String?
	let boro: String?
	let overviewParagraph: String?
	let school10thSeats: String?
	let academicopportunities1: String?
	let academicopportunities2: String?
	let ellPrograms: String?
	let buildingCode: String?
	let location: String?
	let phoneNumber: String?
	let faxNumber: String?
	let schoolEmail: String?
	let subway: String?
	let bus: String?
	let website: String?
	let grades2018: String?
	let finalgrades: String?
	let totalStudents: String?
	let extracurricularActivities: String?
	let schoolSports: String?
	let attendanceRate: String?
	let admissionspriority11: String?
	let admissionspriority21: String?
	let admissionspriority31: String?
	let primaryAddressLine1: String?
	let city: String?
	let zip: String?
	let stateCode: String?
	let latitude: String?
	let longitude: String?
	let nta: String?
	let borough: String?

	init(dbn: String, schoolName: String?) {
		self.dbn = dbn
		self.schoolName = schoolName
		self.boro = nil
		self.overviewParagraph = nil
		self.school10thSeats = nil
		self.academicopportunities1 = nil
		self.academicopportunities2 = nil
		self.ellPrograms = nil
		self.buildingCode = nil
		self.location = nil
		self.phoneNumber = nil
		self.faxNumber = nil
		self.schoolEmail = nil
		self.subway = nil
		self.bus = nil
		self.website = nil
		self.grades2018 = nil
		self.finalgrades = nil
		self.totalStudents = nil
		self.extracurricularActivities = nil
		self.schoolSports = nil
		self.attendanceRate = nil
		self.admissionspriority11 = nil
		self.admissionspriority21 = nil
		self.admissionspriority31 = nil
		self.primaryAddressLine1 = nil
		self.city = nil
		self.zip = nil
		self.stateCode = nil
		self.latitude = nil
		self.longitude = nil
		self.nta = nil
		self.borough = nil
	}
}
