//
//  PaddingLabel.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import UIKit

@IBDesignable class PaddingLabel: UILabel {

	private let topInset: CGFloat = 2
	private let bottomInset: CGFloat = 2
	private let leftInset: CGFloat = 4
	private let rightInset: CGFloat = 4

	override func drawText(in rect: CGRect) {
		let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
		super.drawText(in: rect.inset(by: insets))
	}

	override var intrinsicContentSize: CGSize {
		let size = super.intrinsicContentSize
		return CGSize(width: size.width + leftInset + rightInset,
					  height: size.height + topInset + bottomInset)
	}

	override var bounds: CGRect {
		didSet {
			preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
		}
	}
}
