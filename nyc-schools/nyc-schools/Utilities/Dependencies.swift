//
//  Dependencies.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation

class Dependencies {

	private init() { }
	public static let apiClient = APIClient()
}

