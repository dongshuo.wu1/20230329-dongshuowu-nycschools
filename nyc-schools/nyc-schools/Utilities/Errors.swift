//
//  Errors.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/29/23.
//

import Foundation

enum Errors: Error {
	case networkError(Error)
	case serverError(Int)
	case noData
	case decodingError(Error)
	case encodingError(Error)
}

