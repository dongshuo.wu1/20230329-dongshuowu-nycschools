//
//  UIColor+Extensions.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation
import UIKit

extension UIColor {
	static let manhattanGreen = UIColor(red: 110/256, green: 191/256, blue: 124/256, alpha: 1)
	static let brooklynYellow = UIColor(red: 244/256, green: 224/256, blue: 102/256, alpha: 1)
	static let queensOrange = UIColor(red: 223/256, green: 145/256, blue: 53/256, alpha: 1)
	static let bronxRed = UIColor(red: 221/256, green: 94/256, blue: 86/256, alpha: 1)
	static let statenIslandPurple = UIColor(red: 186/256, green: 172/256, blue: 215/256, alpha: 1)
	static let tableViewBackground = UIColor(red: 224/256, green: 224/256, blue: 224/256, alpha: 1)
}
