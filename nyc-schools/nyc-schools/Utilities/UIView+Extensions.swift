//
//  UIView+Extension.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/31/23.
//

import Foundation
import UIKit

extension UIView {
	
	func anchorNoSpace(in view: UIView) {
		translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			self.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
			self.trailingAnchor.constraint(equalTo: view.trailingAnchor)
		])
	}
	
	func anchor(in view: UIView,
				top: CGFloat? = nil,
				left: CGFloat? = nil,
				bottom: CGFloat? = nil,
				right: CGFloat? = nil) {
		translatesAutoresizingMaskIntoConstraints = false
		var constraints: [NSLayoutConstraint] = []
		
		if let top = top {
			constraints.append(topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: top))
		}
		
		if let left = left {
			constraints.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: left))
		}
		
		if let bottom = bottom {
			constraints.append(bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -bottom))
		}
		
		if let right = right {
			constraints.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -right))
		}
		
		NSLayoutConstraint.activate(constraints)
	}
	
}
