//
//  APIService.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/29/23.
//

import Foundation

protocol APIServiceProtocol {
	func loadData<T: Codable>(model: T.Type,
							  request: URLRequest,
							  queue: DispatchQueue,
							  _ completion: @escaping ((Result<[T], Errors>) -> Void))
}

class APIClient: APIServiceProtocol {
	
	private let session: URLSession
	private let decoder: JSONDecoder
	
	
	init(session: URLSession = .shared,
		 decoder: JSONDecoder = JSONDecoder()) {
		self.session = session
		self.decoder = decoder
		decoder.keyDecodingStrategy = .convertFromSnakeCase
	}
	
	func loadData<T: Codable>(model: T.Type,
							  request: URLRequest,
							  queue: DispatchQueue = .main,
							  _ completion: @escaping ((Result<[T], Errors>) -> Void)) {

		session.dataTask(with: request) { data, response, error in
			
			if let error = error {
				queue.async {
					completion(.failure(.networkError(error)))
				}
			}
			
			if let response = response as? HTTPURLResponse,
			   !(200...299).contains(response.statusCode) {
				queue.async {
					completion(.failure(.serverError(response.statusCode)))
				}
			}
			
			guard let data = data else {
				queue.async {
					completion(.failure(.noData))
				}
				return
			}

			do {
				let arrayResource = try self.decoder.decode([T].self, from: data)
				queue.async {
					completion(.success(arrayResource))
				}
			} catch {
				queue.async {
					completion(.failure(.decodingError(error)))
					print(error.localizedDescription)
				}
			}
		}.resume()
	}
}
