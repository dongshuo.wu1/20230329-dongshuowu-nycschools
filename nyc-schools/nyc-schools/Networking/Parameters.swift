//
//  Parameters.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation

struct Parameters {
	
	struct SQL {
		static let select = "$select"
		static let limit = "$limit"
		static let offset = "$offset"
	}

	static let dbn = "dbn"
}
