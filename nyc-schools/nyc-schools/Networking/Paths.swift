//
//  Paths.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation

class URLString {
	static let schoolListPath: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
	static let satScoreListPath: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
