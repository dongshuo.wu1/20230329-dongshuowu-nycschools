//
//  SchoolAttributes.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation
import UIKit

enum Borough: String {
	case manhattan = "MANHATTAN"
	case brooklyn = "BROOKLYN"
	case queens = "QUEENS"
	case bronx = "BRONX"
	case statenIsland = "STATEN IS"
}

extension Borough {
	var color: UIColor {
		switch self {
		case .manhattan:
			return .manhattanGreen
		case .brooklyn:
			return .brooklynYellow
		case .queens:
			return .queensOrange
		case .bronx:
			return .bronxRed
		case .statenIsland:
			return .statenIslandPurple
		}
	}
}
