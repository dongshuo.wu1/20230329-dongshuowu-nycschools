//
//  SchoolDetailsViewController.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/31/23.
//

import Foundation
import UIKit

class SchoolDetailsViewController: UIViewController {
	
	private let reuseIdentifier: String = "reuseIdentifier"
	
	private var viewModel: SchoolListViewable
	
	private lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.alwaysBounceVertical = true
		tableView.delegate = self
		tableView.dataSource = self
		tableView.backgroundColor = .tableViewBackground
		tableView.allowsSelection = false
		return tableView
	}()

	init(viewModel: SchoolListViewable) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setUpViews()
		tableView.register(SchoolDetailsCellView.self, forCellReuseIdentifier: reuseIdentifier)
		viewModel.updateSchoolDetailsView = { [weak self] in
			self?.tableView.reloadData()
		}
		viewModel.fetchSchoolDetails()
		viewModel.fetchSATScore()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		configureNavigationController()
	}
	
	private func setUpViews() {
		view.backgroundColor = .white
		view.addSubview(tableView)
		tableView.anchorNoSpace(in: view)
	}

	private func configureNavigationController() {
		navigationItem.title = "School Details"
		navigationController?.navigationBar.prefersLargeTitles = false
		navigationController?.navigationBar.tintColor = .black
	}
}

extension SchoolDetailsViewController: UITableViewDataSource, UITableViewDelegate {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? SchoolDetailsCellView else {
			return UITableViewCell()
		}
		cell.bind(attributedString: viewModel.schoolDetailsAttributedString())
		return cell
	}
}
