//
//  SchoolDetailsCellView.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/31/23.
//

import Foundation
import UIKit

class SchoolDetailsCellView: UITableViewCell {
	
	let contentLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.sizeToFit()
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setUpViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setUpViews() {
		addSubview(contentLabel)
		contentLabel.anchor(in: self, top: 10, left: 10, bottom: 10, right: 10)
	}
	
	func bind(attributedString: NSAttributedString) {
		contentLabel.attributedText = attributedString
	}
}
