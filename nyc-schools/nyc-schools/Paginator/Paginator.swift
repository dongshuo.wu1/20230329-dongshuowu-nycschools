//
//  Paginator.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 4/1/23.
//

import Foundation

/**
 The class that conforms to this protocol will assit the view model class to properly paginate the data results.
 Given more time, I would add an enum Direction forward and backword, so that we can keep a certain amount of data in the memory, say 100. And get rid of oldest data if there are more than 100.
 */

protocol PaginatorProtocol {
	
	var pageSize: Int { get }
	var offSet: Int { get }
	func prepareForNextPage()
	func reset()
}

class Paginator: PaginatorProtocol {
	
	private var currentPage: Int = 0
	
	let pageSize: Int = 20
	
	var offSet: Int {
		currentPage * pageSize
	}
	
	func prepareForNextPage() {
		currentPage += 1
	}
	
	func reset() {
		currentPage = 0
	}
}
