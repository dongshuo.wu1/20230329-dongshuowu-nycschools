//
//  SchoolListViewModel.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation
import UIKit

protocol SchoolListViewable {
	var numberOfItems: Int { get }
	
	/**
	 This is the callback for view controller to update the list view.
	 Given more time, I would create a generic data resource class that is reactive, in which case we would not need a specific callback.
	 */
	var updateViews: (() -> Void)? { get set }
	
	/**
	 This is the callback for view controller to update the detail view.
	 Given more time, I would create a generic data resource class that is reactive, in which case we would not need a specific callback.
	 */
	var updateSchoolDetailsView: (() -> Void)? { get set }
	
	func schoolListCellViewData(at index: Int) -> SchoolListCellViewData
	func schoolDetailsAttributedString() -> NSAttributedString
	func loadSchools(initialFetch: Bool)
	func setIndexToFetchDetails(to value: Int)
	func fetchSchoolDetails()
	func fetchSATScore()
	func shouldFetchMore(at index: Int) -> Bool
}

class SchoolListViewModel: SchoolListViewable {

	private let apiClient: APIServiceProtocol = APIClient()
	private let paginator: PaginatorProtocol = Paginator()
	private var stopFetching: Bool = false
	
	private var schoolList: [School] = [] {
		didSet {
			self.updateViews?()
		}
	}
	
	private var schoolIndexToFetchForDetails: Int?
	
	private var schoolDetails: School? {
		didSet {
			self.updateSchoolDetailsView?()
		}
	}
	
	private var satScore: SATScore? {
		didSet {
			self.updateSchoolDetailsView?()
		}
	}
	
	init(schoolList: [School] = []) {
		self.schoolList = schoolList
	}
	
	var numberOfItems: Int {
		schoolList.count
	}
	
	var updateViews: (() -> Void)?
	var updateSchoolDetailsView: (() -> Void)?
	
	private func dbn(at index: Int) -> String? {
		guard index < schoolList.count else {
			return nil
		}
		return schoolList[index].dbn
	}
	
	private func schoolName(at index: Int) -> String? {
		guard index < schoolList.count else {
			return nil
		}
		return schoolList[index].schoolName?.trimmingCharacters(in: .whitespaces)
	}
	
	private func boroughRawString(at index: Int) -> String? {
		guard index < schoolList.count else {
			return nil
		}
		return schoolList[index].borough?.trimmingCharacters(in: .whitespaces)
	}
	
	private func schoolBorough(at index: Int) -> String? {
		boroughRawString(at: index)?.capitalized
	}
	
	private func boroughColor(at index: Int) -> UIColor? {
		guard let rawString = boroughRawString(at: index) else {
			return nil
		}
		return Borough(rawValue: rawString)?.color
	}
	
	private func grades(at index: Int) -> String? {
		guard index < schoolList.count else {
			return nil
		}
		return "Grades: " + (schoolList[index].finalgrades?.trimmingCharacters(in: .whitespaces) ?? "")
	}
	
	func schoolDetailsAttributedString() -> NSAttributedString {
		let attributedString = NSMutableAttributedString()
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineSpacing = 2
		
		let bold16 = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)]
		let system14 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
		let bold14 = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]
		
		if let schoolName = schoolDetails?.schoolName {
			let schoolNameAttributedString = NSAttributedString(string: schoolName + "\n\n", attributes: bold16)
			attributedString.append(schoolNameAttributedString)
		}
		
		if let phoneNumber = schoolDetails?.phoneNumber {
			let phoneNumberAttributedString = NSAttributedString(string: "Phone: " + phoneNumber + "\n", attributes: bold14)
			attributedString.append(phoneNumberAttributedString)
		}
		
		if let faxNumber = schoolDetails?.faxNumber {
			let faxNumberAttributedString = NSAttributedString(string: "Fax: " + faxNumber + "\n", attributes: bold14)
			attributedString.append(faxNumberAttributedString)
		}
		
		if let schoolEmail = schoolDetails?.schoolEmail {
			let emailAttributedString = NSAttributedString(string: "Email: " + schoolEmail + "\n", attributes: bold14)
			attributedString.append(emailAttributedString)
		}
		
		if let website = schoolDetails?.website {
			let websiteAttributedString = NSAttributedString(string: "Website: " + website + "\n\n", attributes: bold14)
			attributedString.append(websiteAttributedString)
		}
		
		if let overviewParagraph = schoolDetails?.overviewParagraph {
			let overviewParagraphAttributedString = NSAttributedString(string: overviewParagraph + "\n\n", attributes: system14)
			attributedString.append(overviewParagraphAttributedString)
		}
		
		let academicOpportunitiesLiteral = "Academic Opportunities:\n"
		let academicOpportunitiesLiteralAttributedString = NSAttributedString(string: academicOpportunitiesLiteral, attributes: bold14)
		attributedString.append(academicOpportunitiesLiteralAttributedString)
		
		if let academicopportunities1 = schoolDetails?.academicopportunities1 {
			let academidopportunities1AttributedString = NSAttributedString(string: academicopportunities1 + "\n", attributes: system14)
			attributedString.append(academidopportunities1AttributedString)
		}
		
		if let academicopportunities2 = schoolDetails?.academicopportunities2 {
			let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
			let academidopportunities2AttributedString = NSAttributedString(string: academicopportunities2 + "\n\n", attributes: attributes)
			attributedString.append(academidopportunities2AttributedString)
		}
		
		let ellProgramsLiteral = "ELL Programs:\n"
		let ellProgramsLiteralAttributedString = NSAttributedString(string: ellProgramsLiteral, attributes: bold14)
		attributedString.append(ellProgramsLiteralAttributedString)
		
		if let ellProgram = schoolDetails?.ellPrograms {
			let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
			let ellProgramAttributedString = NSAttributedString(string: ellProgram + "\n\n", attributes: attributes)
			attributedString.append(ellProgramAttributedString)
		}
		
		if let satScore = satScore {
			attributedString.append(satScoreAttributedString(from: satScore))
		}
		
		return attributedString
	}
	
	func schoolListCellViewData(at index: Int) -> SchoolListCellViewData {
		SchoolListCellViewData(schoolName: schoolName(at: index),
							   borough: schoolBorough(at: index),
							   boroughColor: boroughColor(at: index),
							   grades: grades(at: index))
	}
	
	func loadSchools(initialFetch: Bool = false) {
		
		if initialFetch {
			paginator.reset()
			schoolList = []
		}
		
		guard var url = URL(string: URLString.schoolListPath) else {
			return
		}
		
		url.append(queryItems: [
			URLQueryItem(name: Parameters.SQL.limit, value: "\(paginator.pageSize)"),
			URLQueryItem(name: Parameters.SQL.offset, value: "\(paginator.offSet)"),
			URLQueryItem(name: Parameters.SQL.select, value: "dbn,school_name,borough,finalgrades")
		])
		print(url)
		let request = URLRequest(url: url)
		apiClient.loadData(model: School.self,
						   request: request,
						   queue: .main) { [weak self] results in
			guard let self = self else { return }
			switch results {
			case .success(let schools):
				self.schoolList.append(contentsOf: schools)
				if schools.count < self.paginator.pageSize {
					self.stopFetching = true
				}
				self.paginator.prepareForNextPage()
			case .failure(let error):
				print(error)
			}
		}
	}
	
	func setIndexToFetchDetails(to value: Int) {
		schoolIndexToFetchForDetails = value
	}
	
	func fetchSchoolDetails() {
		guard var url = URL(string: URLString.schoolListPath),
			  let indexToFetch = schoolIndexToFetchForDetails else {
			return
		}
		
		url.append(queryItems: [
			URLQueryItem(name: Parameters.SQL.select, value: "*"),
			URLQueryItem(name: Parameters.dbn, value: dbn(at: indexToFetch))
		])
		print(url)
		let request = URLRequest(url: url)
		apiClient.loadData(model: School.self,
						   request: request,
						   queue: .main) { [weak self] results in
			guard let self = self else { return }
			switch results {
			case .success(let schools):
				self.schoolDetails = schools.first
				_ = self.schoolDetailsAttributedString()
			case .failure(let error):
				print(error)
			}
		}
	}
	
	func shouldFetchMore(at index: Int) -> Bool {
		index >= schoolList.count - 1 && !stopFetching
	}
	
	func fetchSATScore() {
		guard var url = URL(string: URLString.satScoreListPath),
			  let indexToFetch = schoolIndexToFetchForDetails,
			  let dbn = dbn(at: indexToFetch) else {
			return
		}
		
		url.append(queryItems: [URLQueryItem(name: Parameters.dbn, value: dbn)])
		print(url)
		let request = URLRequest(url: url)
		apiClient.loadData(model: SATScore.self,
						   request: request,
						   queue: .main) { [weak self] results in
			guard let self = self else { return }
			switch results {
			case .success(let satScores):
				self.satScore = satScores.first
				_ = self.schoolDetailsAttributedString()
			case .failure(let error):
				print(error)
			}
		}
	}
	
	private func satScoreAttributedString(from satScore: SATScore) -> NSAttributedString {
		
		let system14 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
		let bold14 = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]
		
		let attrString = NSMutableAttributedString(string: "SAT scores: \n", attributes: bold14)
		
		if let numOfSatTestTakers = satScore.numOfSatTestTakers {
			let attr = NSAttributedString(string: "Number of SAT test takers: " + numOfSatTestTakers + "\n", attributes: system14)
			attrString.append(attr)
		}
		
		if let reading = satScore.satCriticalReadingAvgScore {
			let attr = NSAttributedString(string: "Critical reading average score: " + reading + "\n", attributes: system14)
			attrString.append(attr)
		}
		
		if let math = satScore.satMathAvgScore {
			let attr = NSAttributedString(string: "Math average score: " + math + "\n", attributes: system14)
			attrString.append(attr)
		}
		
		if let writing = satScore.satWritingAvgScore {
			let attr = NSAttributedString(string: "Writing average score: " + writing + "\n", attributes: system14)
			attrString.append(attr)
		}
		
		return attrString
	}
}
