//
//  SchoolListCell.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/30/23.
//

import Foundation
import UIKit

class SchoolListCell: UITableViewCell {
		
	let schoolNameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.font = .boldSystemFont(ofSize: 16)
		return label
	}()
	
	let boroughLabel: PaddingLabel = {
		let label = PaddingLabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 1
		label.font = .systemFont(ofSize: 12)
		label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
		return label
	}()
	
	let gradesLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 0
		label.font = .systemFont(ofSize: 12)
		label.textColor = .lightGray
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setUpViews()
		self.selectionStyle = .none
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setUpViews() {
		addSubview(schoolNameLabel)
		addSubview(boroughLabel)
		addSubview(gradesLabel)
		NSLayoutConstraint.activate([
			schoolNameLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 5),
			schoolNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			schoolNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			gradesLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: 2),
			gradesLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			gradesLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
			boroughLabel.topAnchor.constraint(equalTo: gradesLabel.bottomAnchor, constant: 2),
			boroughLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
			boroughLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -5),
		])
	}
	
	func bind(data: SchoolListCellViewData) {
		schoolNameLabel.text = data.schoolName
		boroughLabel.text = data.borough
		boroughLabel.backgroundColor = data.boroughColor
		gradesLabel.text = data.grades
	}
}

struct SchoolListCellViewData {
	let schoolName: String?
	let borough: String?
	let boroughColor: UIColor?
	let grades: String?
	
	init(schoolName: String?, borough: String?, boroughColor: UIColor?, grades: String?) {
		self.schoolName = schoolName
		self.borough = borough
		self.boroughColor = boroughColor
		self.grades = grades
	}
}
