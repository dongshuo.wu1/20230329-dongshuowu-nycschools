//
//  SchoolListViewController.swift
//  nyc-schools
//
//  Created by Dongshuo Wu on 3/29/23.
//

import UIKit

class SchoolListViewController: UIViewController {
	
	private let reuseIdentifier = "reuseIdentifier"
	private var viewModel: SchoolListViewable
	
	init(viewModel: SchoolListViewable) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		tableView.dataSource = self
		return tableView
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		setUpViews()
		tableView.register(SchoolListCell.self, forCellReuseIdentifier: reuseIdentifier)
		viewModel.updateViews = { [weak self] in
			self?.tableView.reloadData()
		}
		viewModel.loadSchools(initialFetch: true)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		configureNavigationController()
	}
	
	private func configureNavigationController() {
		navigationItem.title = "School List"
		navigationController?.navigationBar.prefersLargeTitles = true
	}
	
	private func setUpViews() {
		view.backgroundColor = .white
		view.addSubview(tableView)
		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
		])
	}
}

/**
 Delegation methods of UITableViewController.
 Given more time, I would like to abstract these into a UITableViewAdaptor which will conform to UITableViewDelegate, UITableViewDataSource as well as other needed protocols. This will make the view controller much more concise.
 Of course, it will depend on the business need. If we use SwiftUI instead of UIKit, it would be a different story.
 */

extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModel.numberOfItems
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? SchoolListCell else {
			return UITableViewCell()
		}
		cell.bind(data: viewModel.schoolListCellViewData(at: indexPath.row))
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let schoolDetailsViewController = SchoolDetailsViewController(viewModel: viewModel)
		viewModel.setIndexToFetchDetails(to: indexPath.row)
		navigationController?.pushViewController(schoolDetailsViewController, animated: true)
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if viewModel.shouldFetchMore(at: indexPath.row) {
			viewModel.loadSchools(initialFetch: false)
		}
	}
}

